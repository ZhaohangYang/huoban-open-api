<?php

namespace HuobanOpenApi\Contracts;

interface HuobanConfigInterface
{
    public function getName() : string|null;
    public function getApiKey() : string|null;
    public function getApiUrl() : string;
}